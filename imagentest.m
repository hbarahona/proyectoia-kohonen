clc  %comandos para eliminar datos antiguos y limpiar terminal
clear all
close all

asd=imread('test3.bmp'); %cambiar nombres para probar otras imagenes
asd=rgb2gray(asd); %imagenes RGB a Escalas de grises

X=[]; % vector de entrada puntos (X,Y)
Y=[]; % vector de entrada


nNeuron=20; %cantidad de neuronas
T=300; %cantidad de iteraciones

umbral=150; % umbral rango 0-255 escala grises  o 1 si es binario

for j=1:length(asd)
    for i=1:length(asd)
        if asd(i,j)<umbral
            X=[X i]; %agregamos cada punto que sobrepase el umbral
            Y=[Y j];
        end
    end
end

x1=X;
x2=Y;
nInputs=length(x1); %numero de entradas segun cantidad de puntos

for j1=1:nNeuron 
    for j2=1:nNeuron
        w1(j1,j2)=(max(x1)-min(x1))*rand+min(x1); %asignacion de pesos aleatorio
        w2(j1,j2)=(max(x2)-min(x2))*rand+min(x2);
    end
end

%parte grafica inicio
figure(1)
plot(x1,x2,'.b')
hold on
plot(w1,w2,'or')
plot(w1,w2,'k','linewidth',1)
plot(w1',w2','k','linewidth',1)
hold off
title('t=0');
drawnow
no=1;
do=10;
t=1; 
%parte grafica fin

while (t<=T)
    n=no*(1-t/T);
    d=round(do*(1-t/T));
    %loop por cada entrada
    for i=1:nInputs
        e_norm=(x1(i)-w1).^2+(x2(i)-w2).^2;
        minj1=1;minj2=1;
        min_norm=e_norm(minj1,minj2);
        for j1=1:nNeuron
            for j2=1:nNeuron
                if e_norm(j1,j2)<min_norm
                    min_norm=e_norm(j1,j2);
                    minj1=j1;
                    minj2=j2;
                end
            end
        end
        j1star= minj1;
        j2star= minj2;
        %actualiza la neurona ganadora
        w1(j1star,j2star)=w1(j1star,j2star)+n*(x1(i)- w1(j1star,j2star));
        w2(j1star,j2star)=w2(j1star,j2star)+n*(x2(i)- w2(j1star,j2star));
        %actualiza el vecindario de neuronas
        for dd=1:1:d
            jj1=j1star-dd;
            jj2=j2star;
            if (jj1>=1)
                w1(jj1,jj2)=w1(jj1,jj2)+n*(x1(i)-w1(jj1,jj2));
                w2(jj1,jj2)=w2(jj1,jj2)+n*(x2(i)-w2(jj1,jj2));
            end
            jj1=j1star+dd;
            jj2=j2star;
            if (jj1<=nNeuron)
                w1(jj1,jj2)=w1(jj1,jj2)+n*(x1(i)-w1(jj1,jj2));
                w2(jj1,jj2)=w2(jj1,jj2)+n*(x2(i)-w2(jj1,jj2));
            end
            jj1=j1star;
            jj2=j2star-dd;
            if (jj2>=1)
                w1(jj1,jj2)=w1(jj1,jj2)+n*(x1(i)-w1(jj1,jj2));
                w2(jj1,jj2)=w2(jj1,jj2)+n*(x2(i)-w2(jj1,jj2));
            end
            jj1=j1star;
            jj2=j2star+dd;
            if (jj2<=nNeuron)
                w1(jj1,jj2)=w1(jj1,jj2)+n*(x1(i)-w1(jj1,jj2));
                w2(jj1,jj2)=w2(jj1,jj2)+n*(x2(i)-w2(jj1,jj2));
            end
        end
    end
    t=t+1;
    figure(1)
    plot(x1,x2,'.b')
    hold on
    plot(w1,w2,'or')
    plot(w1,w2,'k','linewidth',2)
    plot(w1',w2','k','linewidth',2)
    hold off
    title(['t=' num2str(t)]);
    drawnow
    
end

