clc %comandos para eliminar datos antiguos y limpiar terminal
clear all
close all

asd=imread('test3.bmp'); %cambiar nombres para probar otras imagenes
asd=rgb2gray(asd); %imagenes RGB a Escalas de grises

X=[];% vector de entrada
Y=[];% vector de entrada puntos (X,Y) e intencidad Z
Z=[];% vector de entrada
umbral=150; % umbral rango 0-255 escala grises

nNeuron=25; %cantidad de neuronas
T=1000; %cantidad de iteraciones

for j=1:length(asd)
    for i=1:length(asd)
        if asd(i,j)<umbral
            X=[X i]; %agregamos cada punto que sobrepase el umbral
            Y=[Y j];
            Z=[Z asd(i,j)];
        end
    end
end

Z=double(255-Z)
minZ=min(Z)
maxZ=max(Z)

for k=1:length(Z) 
    Z(k)=Z(k)/maxZ; %normalizacion de 0-255 a  0,0-1,0
end


nInputs=length(X);  %numero de entradas segun cantidad de puntos

x1=X;
x2=Y;
x3=Z;
 
for j1=1:nNeuron
    for j2=1:nNeuron
        w1(j1,j2)=(max(x1)-min(x1))*rand+min(x1);   %asignacion de pesos aleatorio
        w2(j1,j2)=(max(x2)-min(x2))*rand+min(x2); %asignacion de pesos aleatorio
        w3(j1,j2)=rand; %asignacion de pesos aleatorio
    end
end
vv=1;

%=====================
 
no=1;
do=12;
t=1;
while (t<=T)
    n=no*(1-t/T);
    d=round(do*(1-t/T));
    %ciclo por cada entrada
    for i=1:nInputs
        e_norm=(x1(i)-w1).^2+(x2(i)-w2).^2+(x3(i)-w3).^2;
        minj1=1;minj2=1;
        min_norm=e_norm(minj1,minj2);
        for j1=1:nNeuron
            for j2=1:nNeuron
                if  e_norm(j1,j2)<min_norm
                    min_norm=e_norm(j1,j2);
                    minj1=j1;
                    minj2=j2;
                end
            end
        end
        j1star=  minj1;
        j2star=  minj2;
        
        
        %actualiza la neurona ganadora
        w1(j1star,j2star)=w1(j1star,j2star)+n*(x1(i)-w1(j1star,j2star));
        w2(j1star,j2star)=w2(j1star,j2star)+n*(x2(i)-w2(j1star,j2star));
        w3(j1star,j2star)=w3(j1star,j2star)+n*(x3(i)-w3(j1star,j2star));
        
        %actualiza el vecindario de neuronas
        
        for dd=1:1:d
            
            jj1=j1star-dd;
            jj2=j2star;
            if (jj1>=1)
                w1(jj1,jj2)=w1(jj1,jj2)+n*(x1(i)-w1(jj1,jj2));
                w2(jj1,jj2)=w2(jj1,jj2)+n*(x2(i)-w2(jj1,jj2));
                w3(jj1,jj2)=w3(jj1,jj2)+n*(x3(i)-w3(jj1,jj2));
            end
            
            jj1=j1star+dd;
            jj2=j2star;
            if (jj1<=nNeuron)
                w1(jj1,jj2)=w1(jj1,jj2)+n*(x1(i)-w1(jj1,jj2));
                w2(jj1,jj2)=w2(jj1,jj2)+n*(x2(i)-w2(jj1,jj2));
                w3(jj1,jj2)=w3(jj1,jj2)+n*(x3(i)-w3(jj1,jj2));
            end
            
            jj1=j1star;
            jj2=j2star-dd;
            if (jj2>=1)
                w1(jj1,jj2)=w1(jj1,jj2)+n*(x1(i)-w1(jj1,jj2));
                w2(jj1,jj2)=w2(jj1,jj2)+n*(x2(i)-w2(jj1,jj2));
                w3(jj1,jj2)=w3(jj1,jj2)+n*(x3(i)-w3(jj1,jj2));
            end
            
            jj1=j1star;
            jj2=j2star+dd;
            if (jj2<=nNeuron)
                w1(jj1,jj2)=w1(jj1,jj2)+n*(x1(i)-w1(jj1,jj2));
                w2(jj1,jj2)=w2(jj1,jj2)+n*(x2(i)-w2(jj1,jj2));
                w3(jj1,jj2)=w3(jj1,jj2)+n*(x3(i)-w3(jj1,jj2));
            end
        end
    end
    t=t+1;
    
    
    % inicio parte grafica
    figure(1)
    plot3(x1,x2,x3,'.b')
    hold on
    plot3(w1,w2,w3,'or')
    surf(w1,w2,w3,'FaceColor','interp',...
        'EdgeColor','k',...
        'FaceLighting','phong','linewidth',2)
    axis ([0,64,0,64,0,1])
    view(vv,35)
    vv=vv+1;
    camlight left
    hold off
    title(['t=' num2str(t)])
    drawnow
    
end
 
for zz=1:200
    figure(1)
    plot3(x1,x2,x3,'.b')
    hold on
    plot3(w1,w2,w3,'or')
    surf(w1,w2,w3,'FaceColor','interp',...
        'EdgeColor','k',...
        'FaceLighting','phong','linewidth',2)
    axis([0,64,0,64,0,1])
    view(vv,35)
    vv=vv+1;
    camlight left
    hold off
    title(['t=' num2str(t)])
    drawnow
    
end
% fin parte grafica